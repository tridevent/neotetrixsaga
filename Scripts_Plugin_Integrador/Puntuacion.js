﻿import UnityEngine.UI;
static var puntuacionesTime:int[];
static var puntuacionesScore:int[];
public var puntosTime:Text[];
public var puntosScore:Text[];
public var tituloTime:Text;
public var tituloScore:Text;
static var puntajeTime:int;
static var puntajeScore:int;
private var i:int;
private var j:int;
private var aux:int;
private var aux2:int;
private var aux3:int;
private var aux4:int;

function Start () {
if(Manager.modalidad==false)
	{
	PuntosTime();
	}
if(Manager.modalidad==true)
	{
	PuntosScore();
	}
var texto1:String;
var texto2:String;
var texto3:String;
var texto4:String;
if(seleccionIdiomas.idiomaActual==0)
	{
	texto1="Classic";
	texto2="My Score";
	texto3="Rating";
	texto4="Time";
	tituloScore.GetComponent.<Text>().text=texto1;
	tituloTime.GetComponent.<Text>().text=texto4;
	}
if(seleccionIdiomas.idiomaActual==1)
	{
	texto1="Clasico";
	texto2="Mis Puntos";
	texto3="Puesto";
	texto4="Tiempo";
	tituloScore.GetComponent.<Text>().text=texto1;
	tituloTime.GetComponent.<Text>().text=texto4;
	}
for(var i:int=0;i<5;i++)
	{
	if(puntuacionesScore[i]==puntajeScore && Manager.modalidad==true)
		{
			puntosScore[i].GetComponent.<Text>().text=texto2+(i-1)+" "+ puntuacionesScore[i].ToString();
			puntosTime[i].GetComponent.<Text>().text=texto3+(i-1)+" "+ puntuacionesTime[i].ToString();
		}
		else
		{
			if(puntuacionesTime[i]==puntajeTime && Manager.modalidad==false)
				{
					puntosTime[i].GetComponent.<Text>().text=texto2+(i-1)+" "+ puntuacionesTime[i].ToString();
					puntosScore[i].GetComponent.<Text>().text=texto3+(i-1)+" "+ puntuacionesScore[i].ToString();
				}
			else
				{
				puntosTime[i].GetComponent.<Text>().text=texto3+(i-1)+" "+ puntuacionesTime[i].ToString();
				puntosScore[i].GetComponent.<Text>().text=texto3+(i-1)+" "+ puntuacionesScore[i].ToString();
				}
		}
	}
}
//**************************************************************************************************************************************
function PuntosTime(){
for(i=0;i<5;i++)
	{
if(i==0)
	{
	if(puntajeTime>puntuacionesTime[i])
		{
		if(puntuacionesTime[i]>0)
		{
		if(puntuacionesTime[i+1]>0 && puntajeTime>puntuacionesTime[i+1])
			{
			aux=puntuacionesTime[i+1];
			puntuacionesTime[i+1]=puntuacionesTime[i];
			for(j=0;j<5;j++)
				{
				if(aux>puntuacionesTime[j+1] && aux<puntuacionesTime[j])
							{
							aux2=puntuacionesTime[j+1];
							puntuacionesTime[j+1]=aux;
							if(aux2>puntuacionesTime[j+2] && aux2<puntuacionesTime[j+1])
								{
								aux3=puntuacionesTime[j+2];
								puntuacionesTime[j+2]=aux2;
								if(aux3>puntuacionesTime[j+3] && aux3<puntuacionesTime[j+2])
									{
									aux4=puntuacionesTime[j+3];
									puntuacionesTime[j+3]=aux3;
									if(aux4>puntuacionesTime[j+4] && aux4<puntuacionesTime[j+3])
										{
										puntuacionesTime[j+4]=aux4;
										}
									}
								}
							}
				}
			}
			else
			{
			puntuacionesTime[i+1]=puntuacionesTime[i];
			}
			puntuacionesTime[i]=puntajeTime;
		}
		else
		{
		puntuacionesTime[i]=puntajeTime;
		}
		}
	}
if(i>0)
	{
	if(puntajeTime>puntuacionesTime[i] && puntajeTime<puntuacionesTime[i-1])
		{
		if(puntuacionesTime[i]>0)
		{
		if(puntuacionesTime[i+1]>0 && puntajeTime>puntuacionesTime[i+1])
			{
			aux=puntuacionesTime[i+1];
			puntuacionesTime[i+1]=puntuacionesTime[i];
			for(j=0;j<5;j++)
				{
				if(aux>puntuacionesTime[j+1] && aux<puntuacionesTime[j])
							{
							aux2=puntuacionesTime[j+1];
							puntuacionesTime[j+1]=aux;
							if(aux2>puntuacionesTime[j+2] && aux2<puntuacionesTime[j+1])
								{
								aux3=puntuacionesTime[j+2];
								puntuacionesTime[j+2]=aux2;
								if(aux3>puntuacionesTime[j+3] && aux3<puntuacionesTime[j+2])
									{
									aux4=puntuacionesTime[j+3];
									puntuacionesTime[j+3]=aux3;
									if(aux4>puntuacionesTime[j+4] && aux4<puntuacionesTime[j+3])
										{
										puntuacionesTime[j+4]=aux4;
										}
									}
								}
							}
				}
			}
			else
			{
			puntuacionesTime[i+1]=puntuacionesTime[i];
			}
			puntuacionesTime[i]=puntajeTime;
		}
		else
		{
		puntuacionesTime[i]=puntajeTime;
		}
		}
	}
	}
	PlayerPrefs.SetInt("PuntuacionesTime0",puntuacionesTime[0]);
	PlayerPrefs.SetInt("PuntuacionesTime1",puntuacionesTime[1]);
	PlayerPrefs.SetInt("PuntuacionesTime2",puntuacionesTime[2]);
	PlayerPrefs.SetInt("PuntuacionesTime3",puntuacionesTime[3]);
	PlayerPrefs.SetInt("PuntuacionesTime4",puntuacionesTime[4]);
}
//----------------------------------------------------------------------------------------------------//
function PuntosScore(){
for(i=0;i<5;i++)
	{
if(i==0)
	{
	if(puntajeScore>puntuacionesScore[i])
		{
		if(puntuacionesScore[i]>0)
		{
		if(puntuacionesScore[i+1]>0 && puntajeScore>puntuacionesScore[i+1])
			{
			aux=puntuacionesScore[i+1];
			puntuacionesScore[i+1]=puntuacionesScore[i];
			for(j=0;j<5;j++)
				{
				if(aux>puntuacionesScore[j+1] && aux<puntuacionesScore[j])
							{
							aux2=puntuacionesScore[j+1];
							puntuacionesScore[j+1]=aux;
							if(aux2>puntuacionesScore[j+2] && aux2<puntuacionesScore[j+1])
								{
								aux3=puntuacionesScore[j+2];
								puntuacionesScore[j+2]=aux2;
								if(aux3>puntuacionesScore[j+3] && aux3<puntuacionesScore[j+2])
									{
									aux4=puntuacionesScore[j+3];
									puntuacionesScore[j+3]=aux3;
									if(aux4>puntuacionesScore[j+4] && aux4<puntuacionesScore[j+3])
										{
										puntuacionesScore[j+4]=aux4;
										}
									}
								}
							}
				}
			}
			else
			{
			puntuacionesScore[i+1]=puntuacionesScore[i];
			}
			puntuacionesScore[i]=puntajeScore;
		}
		else
		{
		puntuacionesScore[i]=puntajeScore;
		}
		}
	}
if(i>0)
	{
	if(puntajeScore>puntuacionesScore[i] && puntajeScore<puntuacionesScore[i-1])
		{
		if(puntuacionesScore[i]>0)
		{
		if(puntuacionesScore[i+1]>0 && puntajeScore>puntuacionesScore[i+1])
			{
			aux=puntuacionesScore[i+1];
			puntuacionesScore[i+1]=puntuacionesScore[i];
			for(j=0;j<5;j++)
				{
				if(aux>puntuacionesScore[j+1] && aux<puntuacionesScore[j])
							{
							aux2=puntuacionesScore[j+1];
							puntuacionesScore[j+1]=aux;
							if(aux2>puntuacionesScore[j+2] && aux2<puntuacionesScore[j+1])
								{
								aux3=puntuacionesScore[j+2];
								puntuacionesScore[j+2]=aux2;
								if(aux3>puntuacionesScore[j+3] && aux3<puntuacionesScore[j+2])
									{
									aux4=puntuacionesScore[j+3];
									puntuacionesScore[j+3]=aux3;
									if(aux4>puntuacionesScore[j+4] && aux4<puntuacionesScore[j+3])
										{
										puntuacionesScore[j+4]=aux4;
										}
									}
								}
							}
				}
			}
			else
			{
			puntuacionesScore[i+1]=puntuacionesScore[i];
			}
			puntuacionesScore[i]=puntajeScore;
		}
		else
		{
		puntuacionesScore[i]=puntajeScore;
		}
		}
	}
	}
	PlayerPrefs.SetInt("nivelJugador",global.nivelJugador);
	PlayerPrefs.SetInt("puntajeAcumulado",global.puntajeAcumulado);
	PlayerPrefs.SetInt("PuntuacionesScore0",puntuacionesScore[0]);
	PlayerPrefs.SetInt("PuntuacionesScore1",puntuacionesScore[1]);
	PlayerPrefs.SetInt("PuntuacionesScore2",puntuacionesScore[2]);
	PlayerPrefs.SetInt("PuntuacionesScore3",puntuacionesScore[3]);
	PlayerPrefs.SetInt("PuntuacionesScore4",puntuacionesScore[4]);
}