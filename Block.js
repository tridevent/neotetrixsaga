#pragma strict
var bajar:AudioClip;
var golpear:AudioClip;
var touch:Touch;
static var rnd:int=0;
/*public var izquierdabtn: GameObject;
public var derechabtn: GameObject;
public var girarbtn: GameObject;
public var tirarbtn: GameObject;*/
private var Pulsadoizq : boolean = false;
private var Pulsadoder : boolean = false;
private var Pulsadogir : boolean = false;
private var Pulsadotir : boolean = false;
private var check:boolean=false;
private var check2:boolean=false;
private var check3:boolean=false;
static var check4:boolean=false;
static var check5:boolean=false;
//----------------------------------------Swipe-------------------------//
var tiempoMaximo:float=500;//500
private var minimaDistancia:float=50;//100
var puedeSwipe:boolean;
var direccionSwipe:float=0;
var direccionSwipe2:float=0;
var prueba:Vector2;
var prueba2:Vector2;
var posicionInicial:Vector2;
var distanciaSwipe2 = (posicionInicial).magnitude;
var distanciaSwipe = (posicionInicial).magnitude;
//var tiempoSwipe:float=0;
//var distanciaSwipe:float=0;
//--------------------------------------------
	
var block : String[];

private var blockMatrix : boolean[,];
private var fallSpeed : float;
private var yPosition : int;
private var xPosition : int;
private var size : int;
private var halfSize : int;
private var halfSizeFloat : float;
private var dropped = false;

function Start () {
tiempoMaximo=500;
check=false;
check2=false;
check3=false;
//check4=false;
/*izquierdabtn= GameObject.Find("izquierda");
derechabtn= GameObject.Find("derecha");
girarbtn= GameObject.Find("girar");
tirarbtn= GameObject.Find("tirar");*/
	// Sanity checking
	size = block.Length;
	var width = block[0].Length;
	if (size < 2) {
		Debug.LogError ("Blocks must have at least two lines");
		return;
	}
	if (width != size) {
		Debug.LogError ("Block width and height must be the same");
		return;
	}
	if (size > Manager.use.maxBlockSize) {
		Debug.LogError ("Blocks must not be larger than " + Manager.use.maxBlockSize);
		return;
	}
	for (var i = 1; i < size; i++) {
		if (block[i].Length != block[i-1].Length) {
			Debug.LogError ("All lines in the block must be the same length");
			return;
		}
	}
	
	halfSize = size/2;
	halfSizeFloat = size*.5; // medio tamaño es un número entero de la matriz, pero necesitamos un flotador para colocar los cubos de la pantalla (para tamaños impares)
	
	// Convertir bloque de matriz de cadena del inspector en una matriz 2D boolean para un uso más fácil
	blockMatrix = new boolean[size, size];
	rnd=Random.Range(0,5);
	for (var y = 0; y < size; y++) {
		for (var x = 0; x < size; x++) {
			if (block[y][x] == "1"[0]) {
				blockMatrix[x, y] = true;
				if(rnd==0)
					{
					var block0 = Instantiate(Manager.use.cube0, Vector3(x-halfSizeFloat, (size-y)+halfSizeFloat-size, 0.0), Quaternion.identity) as Transform;
					block0.parent = transform;
					}
				if(rnd==1)
					{
					var block1 = Instantiate(Manager.use.cube1, Vector3(x-halfSizeFloat, (size-y)+halfSizeFloat-size, 0.0), Quaternion.identity) as Transform;
					block1.parent = transform;
					}
				if(rnd==2)
					{
					var block2 = Instantiate(Manager.use.cube2, Vector3(x-halfSizeFloat, (size-y)+halfSizeFloat-size, 0.0), Quaternion.identity) as Transform;
					block2.parent = transform;
					}
				if(rnd==3)
					{
					var block3 = Instantiate(Manager.use.cube3, Vector3(x-halfSizeFloat, (size-y)+halfSizeFloat-size, 0.0), Quaternion.identity) as Transform;
					block3.parent = transform;
					}
				if(rnd==4)
					{
					var block4 = Instantiate(Manager.use.cube4, Vector3(x-halfSizeFloat, (size-y)+halfSizeFloat-size, 0.0), Quaternion.identity) as Transform;
					block4.parent = transform;
					}
					
			}
		}
	}
	GetComponent.<AudioSource>().PlayOneShot(golpear);	
	// Para bloques con tamaños incluso, vamos a sumar 0, pero los tamaños impares necesitamos 0.5 añade a la posición a trabajar de
	transform.position.x = Manager.use.FieldWidth/2 + (size%2 == 0? 0.0 : .5);
	xPosition = transform.position.x - halfSizeFloat;
	yPosition = Manager.use.FieldHeight - 1;
	transform.position.y = yPosition - halfSizeFloat;
	fallSpeed = Manager.use.blockNormalSpeed;
	
	// Compruebe si este bloque sería superponen bloques existentes, en cuyo caso el juego ha terminado
	if (Manager.use.CheckBlock (blockMatrix, xPosition, yPosition)) {
		Manager.use.GameOver();
		return;
	}
	CheckInput();
	yield Delay( (1.0 / Manager.use.blockNormalSpeed) * 2.0 );
	Fall();
}

// Este se utiliza en lugar de WaitForSeconds, de manera que el retardo puede ser interrumpida si el jugador pulsa el botón de gota
function Delay (time : float) {
	var t = 0.0;
	while (t <= time && !dropped) {
		t += Time.deltaTime;	
		yield;
	}
}

function Fall () {
	while (true) {
		// Compruebe si está bloqueado podría colisionar si se mueve una fila hacia abajo
		yPosition--;
		if (Manager.use.CheckBlock (blockMatrix, xPosition, yPosition)) {
			Manager.use.SetBlock (blockMatrix, xPosition, yPosition+1);
			Destroy(gameObject);
			break;
		}
		
		// Hacer caer el bloque de la pantalla hacia abajo 1 plaza
		// También sirve como un retraso ... si quieres a la antigua plaza por plaza movimiento, reemplace esto con WaitForSeconds rendimiento
		for (var i : float = yPosition+1; i > yPosition; i -= Time.deltaTime*fallSpeed) {
			transform.position.y = i - halfSizeFloat;
			yield;
		}
	}
}
function CheckInput () {
while (true) 
	{
		var input = Input.GetAxis("Horizontal");
		if (input < 0.0) {
			yield MoveHorizontal(-1);
			}
		else if (input > 0.0) {
			yield MoveHorizontal(1);
		}
		if (Input.GetButtonDown("Rotate")) {
			RotateBlock();
		}
		if (Input.GetButtonDown("Drop") ||check5==true) {
			fallSpeed = Manager.use.blockDropSpeed;
			dropped = true;
			check5=false;
			GetComponent.<AudioSource>().PlayOneShot(bajar);
			dropped = false;
			break;	// Salir de bucle while, por lo que la co-rutina se detiene (no nos importa acerca de la entrada ya)	
		}
		yield;
		}
	}


function MoveHorizontal (dir:int) {
	// Compruebe si el bloque puede ser movido en la dirección deseada
	if (!Manager.use.CheckBlock (blockMatrix, xPosition + dir, yPosition)) {
		transform.position.x += dir;
		xPosition += dir;
		yield WaitForSeconds (Manager.use.blockMoveDelay);
	}
}

function RotateBlock () {
	// Girar matriz de 90 ° a la derecha y almacenar los resultados en una matriz temporal
	var tempMatrix = new boolean[size, size];
	for (var y = 0; y < size; y++) {
		for (var x = 0; x < size; x++) {
			tempMatrix[y, x] = blockMatrix[x, (size-1)-y];
		}
	}
	
	// Si el bloque rotado no se superpone bloques existentes, copie la matriz rotada hacia atrás y girar el bloque de la pantalla para que coincida con
	if (!Manager.use.CheckBlock (tempMatrix, xPosition, yPosition)) {
		System.Array.Copy (tempMatrix, blockMatrix, size*size);
		transform.Rotate (Vector3.forward * -90.0);
	}
}
//-----------------------------------------------------------------------------------------
function Update () {
	/*if(Input.touchCount > 0)
	    {
	    touch = Input.touches[0];
	        for (var i = 0; i < Input.touchCount; ++i) 
	        {   
	            if(izquierdabtn.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                Pulsadoizq = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
	                //Activar();
	                }                          	           
	            }
	            if(derechabtn.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                Pulsadoder = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
	                //Activar();
	                }                          	           
	            }
	            if(girarbtn.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                Pulsadogir = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
	                //Activar();
	                }                          	           
	            }
	            if(tirarbtn.guiTexture.HitTest(Input.GetTouch(i).position))
	            {        
	                Pulsadotir = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
	                //Activar();
	                }                          	           
	            }
	        }
	    	
	    }*/ 
		/*if (Input.GetMouseButton(0))
		{
	    	if (izquierdabtn.guiTexture.HitTest(Input.mousePosition))
	    	{
	    	Pulsadoizq = true;
	    	}
	    	if(derechabtn.guiTexture.HitTest(Input.mousePosition))
	            {        
	                Pulsadoder = true;                        	           
	            }
	        if(girarbtn.guiTexture.HitTest(Input.mousePosition))
	            {        
	                Pulsadogir = true;                    	           
	            }
	        if(tirarbtn.guiTexture.HitTest(Input.mousePosition))
	            {        
	                Pulsadotir = true;                    	           
	            }
	    }*/
//--------------------------------------------------------------------
if(touch.phase == TouchPhase.Ended || Input.GetMouseButtonUp(0))
	{
	Pulsadoizq = false;
	Pulsadoder = false;
	Pulsadogir = false;
	Pulsadotir = false;
	}
//-------------------------------------------------------------------	
	    if(Pulsadoizq)
		    {
		    izquierda();	
		    }
	    if(Pulsadoder)
	    	{
	    	derecha();
	    	}
	    if(Pulsadogir)
	    	{
	    	girar();
	    	}
	    if(Pulsadotir)
	    	{
	    	print(check4);
	    	if(check4==false && check5==false)
				{
				check5=true;
				check4=true;
				CheckInput ();
				}
	    	}
//---------------------------------------------------------------------	    	
	    if(Pulsadoizq == false)
	    	{
	    	check=false;
	    	} 
	    if(Pulsadoder==false)         		    	
	    	{
	    	check2=false;
	    	}
	    if(Pulsadogir==false)
	    	{
	    	check3=false;
	    	}
	    if(Pulsadotir==false)
	    	{
	    	check4=false;
	    	}
//////////////////////////////////////////swype///////////////////////////////////
if(Pausa.pausado==false)
	{
	  var count = Input.touchCount;//creamos una variable count que cuente la cantidad de touches

	    if (count > 0)//si hay mas de un toque 
	    {			
	  	var touche : Touch = Input.GetTouch(0);//almacena el toque en la variable touche
	        switch (touche.phase)//comprobamos los diferentes tipos de toques 
	        {
	       //-------------------------------------------
	            case TouchPhase.Began://si el toque es de inicio
	                puedeSwipe = false;//puede swipe lo ponemos en falso
	                posicionInicial= touche.position;//tomamos la posicion del toque como posicion inicial
	                var tiempoInicial = Time.time;// guardamos en una variable el tiempo actual del toque
	                break;//salimos del caso

	        //------------------------------------------- 
	            case TouchPhase.Stationary://si el toque es de tipo quedado solo saltarlo por alto
	                break; 
	        //-------------------------------------------

	            case TouchPhase.Ended://si el toque es de tipo final
	            	puedeSwipe = true;// ya puede ser swipe
	                var tiempoSwipe = Time.time -tiempoInicial;//al tiempo actual le restamos el tiempo inicial
	                prueba=touche.position;//tomamos la posicion del toque en donde se finalizo el touch
	                prueba2=touche.position;//una copia de la variable de arriba
	                if(prueba.x > posicionInicial.x)//si la posicion en x es mayor a la posicion inicial en x
	                	{
	                	distanciaSwipe = (prueba.x - posicionInicial.x);//la distancia es la posicion final menos la inicial
	                	}
	                if(prueba.x < posicionInicial.x)//si la posicion en x es menor a la posicion inicial en x
	                	{
	                	distanciaSwipe = (posicionInicial.x - prueba.x);//la distancia es la posicion inicial menos la final
	                	}
	                if(prueba2.y > posicionInicial.y)//si la posicion en y es mayor a la posicion inicial en y
	                	{
	                	distanciaSwipe2 = (prueba2.y - posicionInicial.y);//la distancia es la posicion final menos la inicial
	                	}
	                if(prueba2.y < posicionInicial.y)//si la posicion en y es menor a la posicion inicial en y
	                	{
	                	distanciaSwipe2 = (posicionInicial.y - prueba2.y);//la distancia es la posicion inicial menos la final
	                	}	
	                //distanciaSwipe2 = (prueba2.y - posicionInicial.y).sqrmagnitude;
	                direccionSwipe =Mathf.Sign(posicionInicial.x-prueba.x);//determinamos mediante la funcion matematica mathf
	                direccionSwipe2 =Mathf.Sign(posicionInicial.y-prueba2.y);//la direccion del swipe
	                
	                if (puedeSwipe /*AQUI ESTA LA MODIFICACION "&& (tiempoSwipe < tiempoMaximo)"*/)//si se puede swipe y el tiempo es menor a tiempo maximo 
	                {
	                    // hay swipe
	                    //var direccionSwipe = Mathf.Sign(touche.position.y - posicionInicial.y);
					    if(direccionSwipe>0 && (distanciaSwipe > minimaDistancia))//si la direccion es mayor a 0 y distancia es maoyr a distancia minima
						     {
						     Pulsadoizq=true;
						     }
					    else
					    	{
						    if(direccionSwipe<0 && (distanciaSwipe > minimaDistancia))//si direccion es menor a 0 y la distancia es mayor a distancia minima
							      {
							     Pulsadoder=true;
							      }
							else
								{
							//---------------------------------------------------------------------------------	
							  	if(direccionSwipe2<0 && (distanciaSwipe2 > minimaDistancia))//si direccion es menor a 0 y la distancia es mayor a distancia minima 
								      {
								     Pulsadogir=true;
								      }
								else
									{
								//---------------------------------------------------------------------------------	
									if(direccionSwipe2>0 && (distanciaSwipe2 > minimaDistancia))//si direccion es mayor a 0 y la distancia es mayor a distancia minima
										  {
										  Pulsadotir=true;
										  GetComponent.<AudioSource>().PlayOneShot(bajar);
										  }
									}
								}
							}			
						puedeSwipe = false;
						tiempoSwipe=0;     	
	                }
	                break;
	        }

	    }
	}
}
//----------------------- funciones de controles --------------------------------------------------//
function girar(){
		if(check3==false)
			{
			RotateBlock();
			check3=true;
			}
}
function izquierda(){
		if(check==false)
			{
			MoveHorizontal(-1);
			check=true;
			}
}
function derecha(){
		if(check2==false)
			{
			MoveHorizontal(1);
			check2=true;
			}
}
//----------------------------------------------------------------------------------------------------

function OnGUI()
 {
 //GUI.Label(Rect(100,100,100,100),"" +distanciaSwipe);
 //GUI.Label(Rect(100,150,100,100),"distancia y:" +distanciaSwipe2);
 //GUI.Label(Rect(100,200,100,100),"minima distancia:" +minimaDistancia);
 //GUI.Label(Rect(100,50,100,100),"direccion y:" +direccionSwipe2);
 }
