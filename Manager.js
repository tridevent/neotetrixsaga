import UnityEngine.UI;
var colorCamara: Color;
private var ratePuntos:float=1;
private var tiempoAux:float=100;
private var cambiovel:boolean=false;
var tiempoHUD: Text;
var puntosHUD: Text;
private var texto1:String;
private var texto2:String;
var particulas:Transform;
var particulas_audio:AudioClip;
var Fuente: Font;
var camara:Transform;
private var Tamanofuente:int=16;
private var Estilo= new GUIStyle();
var Fila:AudioClip;
var GanarNivel:AudioClip;
static var modalidad:boolean=false;//false time true score
var dificultad:int=2;
private var copiadificultad:int=0;
static var puntos:int=0;
static var timer:int=0;
private var timer2:int=0;
private var timer3:int=0;
private var timer4:int=0;
private var contadortimer:int=0;
var SubirNivel:GameObject;
private var lineasBorradas:int=0;
private var contadorBloques:int=0;
var tipoModalidad:boolean=false;//false time true score
var _fieldWidth = 10;
var _fieldHeight = 13;
var maxBlockSize = 5;
var blockNormalSpeed = 2.0;
var blockDropSpeed = 30.0;
var blockMoveDelay = .5;
var rowsClearedToSpeedup = 10;
var speedupAmount = .5;
var blocks : GameObject[];
var cube0 : Transform;
var cube1 : Transform;
var cube2 : Transform;
var cube3 : Transform;
var cube4 : Transform;
var leftWall : Transform;
var rightWall : Transform;

private var fieldWidth : int;
private var fieldHeight : int;
private var field : boolean[,];
private var cubeReferences : Transform[];
private var cubePositions : int[];
private var rowsCleared = 0;
static var use : Manager;
//-----------------------------------------------------
/*private var yE:float=0;
private var xP:float=0;
private var yP:float=0;*/
function Start () {
//---------------- modificacion de obtencion de puntos por nivel ---------------
ratePuntos = global.nivelJugador+1;
//----------------------------------------------------------------------------------
tiempoAux= Time.time + 100;
if(seleccionIdiomas.idiomaActual==0)
	{
	texto1="Score: ";
	texto2="Time: ";
	}
if(seleccionIdiomas.idiomaActual==1)
	{
	texto1="Puntos: ";
	texto2="Tiempo: ";
	}
/*//-------------------------------------------------------
yE=100;
xP=25;
yP=120;
//--------------------------------------------------------
Tamanofuente=global.ancho*(Tamanofuente)/760;
xP=global.ancho*(xP)/760;
yP=global.alto*(yP)/600;
yE=global.ancho*yE/760;
//--------------------------------------------------------------*/
//Estilo.normal.textColor = Color.white;
copiadificultad=dificultad;
InvokeRepeating("contando",1,1);
lineasBorradas=0;
contadorBloques=0;
timer=0;
timer2=0;
timer3=0;
timer4=0;
contadortimer=0;
SubirNivel.SetActive(false);
if(tipoModalidad==false)
	{
	modalidad=false;
	}
if(tipoModalidad==true)
	{
	modalidad=true;
	}
	if (!use) {
		use = this;	// Obtenga una referencia a este guiÃ³n, que es una variable estÃ¡tica por eso se usa como un singleton
	}
	else {
		Debug.LogError ("Only one instance of this script is allowed");
		return;
	}
	
	// Hacer que el "real" anchura / altura mÃ¡s grande, para tener en cuenta las fronteras y el espacio en la parte superior para la colocaciÃ³n del primer bloque
	fieldWidth = _fieldWidth + maxBlockSize*2;
	fieldHeight = _fieldHeight + maxBlockSize;
	field = new boolean[fieldWidth, fieldHeight];
	
	/* Hacer las "paredes" y "piso" de la matriz ... usamos true = bloque, y falso = espacio abierto 
De esta forma no necesitamos una lÃ³gica especial para hacer frente a la parte inferior o los bordes del campo de juego, 
desde bloques se chocan con las paredes /suelo de la misma como con otros bloques 
AdemÃ¡s, utilizamos 0 = inferior y fieldHeight-1 = arriba, de modo que las posiciones en las posiciones de los partidos de matriz en el espacio 3D*/
	for (var i = 0; i < fieldHeight; i++) {
		for (var j = 0; j < maxBlockSize; j++) {
			field[j, i] = true;
			field[fieldWidth-1-j, i] = true;
		}
	}
	for (i = 0; i < fieldWidth; i++) {
		field[i, 0] = true;
	}
	
	/*PosiciÃ³n cosas en la escena por lo que se ve bien sin importar lo que los tamaÃ±os estÃ¡n incluidas en el campo de juego 
(Aunque la cÃ¡mara tendrÃ­a que ser trasladado de nuevo para los tamaÃ±os mÃ¡s grandes)*/
	leftWall.position.x = maxBlockSize-.5;
	rightWall.position.x = fieldWidth-maxBlockSize+.5;
	Camera.main.transform.position = Vector3(fieldWidth/2, fieldHeight/2, -16.0);
	
	cubeReferences = new Transform[fieldWidth * fieldHeight];
	cubePositions = new int[fieldWidth * fieldHeight];
	
	SpawnBlock();
}
function ZonaMuerte(){// la velocidad se incrementa bruscamente por un par de segundos
	cambiovel=! cambiovel;
	if(cambiovel==false)
	{
	blockNormalSpeed-=8;
	tiempoAux=Time.time + 100;
	Camera.main.backgroundColor= Color.black;
	}
	else
	{
	blockNormalSpeed+=8;
	Block.check5=true;
	tiempoAux=Time.time + 5;
	Camera.main.backgroundColor= colorCamara;
	}
}
function Update(){
	//print(tiempoAux);
	//print(Time.time);
	if(tiempoAux<Time.time && global.nivelJugador>=5)
	{
		ZonaMuerte();
	}
	tiempoHUD.GetComponent.<Text>().text= texto2 + timer;
	puntosHUD.GetComponent.<Text>().text= texto1 + puntos;
}
function contando () {

timer++;
if(timer3>0)
		{
		timer3++;
		if(timer3>4)
			{
			SubirNivel.SetActive(false);
			timer3=0;
			}
		}
if(timer4>0)
			{
			timer4++;
			if(timer4>2)
				{
				Estilo.normal.textColor = Color.white;
				timer4=0;
				}
			}
if(modalidad==false)
	{
	if(timer2<60)
		{
		timer2++;
		}
	if(timer2==60)
		{
		timer2=0;
		dificultad--;
		GetComponent.<AudioSource>().PlayOneShot(GanarNivel);
		if(dificultad==0)
			{
			blockNormalSpeed += speedupAmount;
			dificultad=copiadificultad;
			}
		SubirNivel.SetActive(true);
		contadortimer=timer;//yield WaitForSeconds (2);
		}
	if((timer-contadortimer)>=5)
			{
			SubirNivel.SetActive(false);
			contadortimer=0;
			}
	}
}

function SpawnBlock () {
	Instantiate (blocks[Random.Range(0, blocks.Length)]);
}

function get FieldHeight () : int {
	return fieldHeight;
}

function get FieldWidth () : int {
	return fieldWidth;
}

/*A ver si la matriz bloque coincidirÃ­a bloques existentes en el campo de juego 
(Cheque de abajo hacia arriba, ya que en el uso general de juego que es un poco mÃ¡s eficiente de esa manera)*/
function CheckBlock (blockMatrix : boolean[,], xPos : int, yPos : int) : boolean {
	var size = blockMatrix.GetLength(0);
	for (var y = size-1; y >= 0; y--) {
		for (var x = 0; x < size; x++) {
			if (blockMatrix[x, y] && field[xPos+x, yPos-y]) {
				return true;
			}
		}
	}
	return false;
}

/* Una funciÃ³n de cÃ³digo auxiliar, ya que los objetos que se destruyen puede ya no tienen corrutinas correr, incluso si la co-rutina estÃ¡ en una escritura diferente 
De esta manera la secuencia de comandos de bloque puede llamar a la co-rutina _SetBlock y continuarÃ¡ funcionando despuÃ©s se destruye el bloque*/
function SetBlock (blockMatrix : boolean[,], xPos : int, yPos : int) {
	_SetBlock (blockMatrix, xPos, yPos);
}

/*Hacer cubos de la pantalla de la posiciÃ³n en el arreglo cuando el bloque se detiene la caÃ­da mÃ¡s 
SÃ³lo utilizando DetachChildren no es factible porque el niÃ±o cubos pueden estar en diferentes orientaciones, 
que puede estropear su posiciÃ³n en el eje Y, que tenemos que ser consistentes en CollapseRow 
Escriba tambiÃ©n la matriz de bloque en la ubicaciÃ³n correspondiente en el campo de juego*/
function _SetBlock (blockMatrix : boolean[,], xPos : int, yPos : int) {
	var size = blockMatrix.GetLength(0);
	for (var y = 0; y < size; y++) {
		for (var x = 0; x < size; x++) {	
			if (blockMatrix[x, y]) {
				//var rnd:int=0;
				//rnd=Random.Range(0,5);
				//Instantiate (cube0, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity); un solo color cuando se establece el bloque
				if(Block.rnd==0)
					{
					Instantiate (cube0, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					Instantiate (particulas, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					GetComponent.<AudioSource>().PlayOneShot(particulas_audio);
					}
				if(Block.rnd==1)
					{
					Instantiate (cube1, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					Instantiate (particulas, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					GetComponent.<AudioSource>().PlayOneShot(particulas_audio);
					}
				if(Block.rnd==2)
					{
					Instantiate (cube2, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					Instantiate (particulas, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					GetComponent.<AudioSource>().PlayOneShot(particulas_audio);
					}
				if(Block.rnd==3)
					{
					Instantiate (cube3, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					Instantiate (particulas, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					GetComponent.<AudioSource>().PlayOneShot(particulas_audio);
					}
				if(Block.rnd==4)
					{
					Instantiate (cube4, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					Instantiate (particulas, Vector3(xPos+x, yPos-y, 0.0), Quaternion.identity);
					GetComponent.<AudioSource>().PlayOneShot(particulas_audio);
					}
				field[xPos+x, yPos-y] = true;
			}
		}
	}
	yield CheckRows (yPos - size, size);
	camara.transform.position.y= camara.transform.position.y-0.5;
	yield WaitForSeconds (0.1);
	camara.transform.position.y= camara.transform.position.y+0.5;
	SpawnBlock();
}

function CheckRows (yStart : int, size : int) {
	yield;	//Espera un marco para el bloque a ser destruido por lo que no incluimos esos cubos
	if (yStart < 1) yStart = 1;	// AsegÃºrese de iniciar por encima del suelo
	for (var y = yStart; y < yStart+size; y++) {
		for (var x = maxBlockSize; x < fieldWidth-maxBlockSize; x++) { // No necesitamos para comprobar las paredes
			if (!field[x, y]) break;
		}
		//Si completÃ³ el ciclo anterior, entonces x se Ancho-max igual campo TamaÃ±o de bloque, lo que significa que la fila se llenÃ³ por completo en
		if (x == fieldWidth-maxBlockSize) {
			yield CollapseRows (y);
			y--; //Queremos comprobar la misma fila de nuevo despuÃ©s de la caÃ­da, en caso de que hubiera mÃ¡s de una fila llena de
		}
	}
}

function CollapseRows (yStart : int) {
	//Mover filas en el array, que borra la fila actual (ystart)
	for (var y = yStart; y < fieldHeight-1; y++) {
		for (var x = maxBlockSize; x < fieldWidth-maxBlockSize; x++) {
			field[x, y] = field[x, y+1];
		}
	}
	// AsegÃºrese de que la lÃ­nea superior se borra
	for (x = maxBlockSize; x < fieldWidth-maxBlockSize; x++) {
		field[x, fieldHeight-1] = false;
	}
	
	// Destruye los cubos de la pantalla en la fila eliminada, y almacenar las referencias a los cubos que estÃ¡n por encima de ella
	var cubes = gameObject.FindGameObjectsWithTag("Cube");
	var cubesToMove = 0;
	for (cube in cubes) {
		if (cube.transform.position.y > yStart) {
			cubePositions[cubesToMove] = cube.transform.position.y;
			cubeReferences[cubesToMove++] = cube.transform;
		}
		else if (cube.transform.position.y == yStart) {
			Destroy(cube);
			puntos=puntos+ratePuntos;// cambiar puntos obtenidos por bloque
			if(contadorBloques<10)
				{
				contadorBloques++;
				}
			if(contadorBloques==10)
				{
				timer4++;
				contadorBloques=0;
				lineasBorradas++;
				GetComponent.<AudioSource>().PlayOneShot(Fila);
				Estilo.normal.textColor = Color.red;
				}
			if(modalidad==true)
				{
				if(lineasBorradas==dificultad)
					{
					SubirNivel.SetActive(true);
					timer3++;
					GetComponent.<AudioSource>().PlayOneShot(GanarNivel);
					lineasBorradas=0;
					blockNormalSpeed += speedupAmount;
					}
				}
		}
	}
	//-----LIMPIEZA!!!----
	System.GC.Collect();
	//--------------------
	/*Mueva los cubos adecuados por un cuadrado 
El tercer parÃ¡metro en Mathf.Lerp se fija a 1,0, lo que hace que la transform.position.y ser posicionado exactamente cuando se hace, 
que es importante para la lÃ³gica del juego (ver el cÃ³digo justo por encima de)*/
	var t = 0.0;
	while (t <= 1.0) {
		t += Time.deltaTime * 5.0;
		for (var i = 0; i < cubesToMove; i++) {
			cubeReferences[i].position.y = Mathf.Lerp (cubePositions[i], cubePositions[i]-1, t);
		}
		yield;
	}
	
	// Haga bloques caen mÃ¡s rÃ¡pido cuando se borran filas suficientes
	/*if (++rowsCleared == rowsClearedToSpeedup) {
		blockNormalSpeed += speedupAmount;
		rowsCleared = 0;
	}*/
}

function GameOver () {
	global.puntajeAcumulado+=puntos;
	print(global.puntajeAcumulado);
	print(global.nivelJugador);
	if(modalidad==false)
		{
		Puntuacion.puntajeTime=puntos;
		puntos=0;
		}
	if(modalidad==true)
		{
		Puntuacion.puntajeScore=puntos;
		puntos=0;
		}
	Application.LoadLevel("Publicidad");
}

// Imprime el estado de la matriz de campo, para la depuraciÃ³n
function PrintField () {
	var fieldChars = "";
	for (var y = fieldHeight-1; y >= 0; y--) {
		for (var x = 0; x < fieldWidth; x++) {
			fieldChars += field[x, y]? "1" : "0";
		}
		fieldChars += "\n";
	}
	Debug.Log (fieldChars);
}

function OnGUI (){

//Estilo.font = Fuente;
//Estilo.fontSize = Tamanofuente;
/*if(seleccionIdiomas.idiomaActual==0)
	{
	texto1="Score: ";
	texto2="Time: ";
	}
if(seleccionIdiomas.idiomaActual==1)
	{
	texto1="Puntos: ";
	texto2="Tiempo: ";
	}
GUI.Label(Rect(xP,yP,yE,yE),texto1+ puntos,Estilo);
GUI.Label(Rect(xP,yP-(yP/4),yE,yE),texto2+ timer,Estilo);*/
}
